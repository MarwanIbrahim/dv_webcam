# dv-webcam

Simple example showing how to setup an inivation event-camera as a webcam on a Linux-based OS using video for linux (
v4l) functionality.

## Dependencies

dv-processing, OpenCV

## Steps

1. Install v4l2-loopback

```bash
sudo apt install v4l2loopback-dkms v4l-utils
```

2. Add a virtual device

```bash
sudo modprobe v4l2loopback devices=1 exclusive_caps=1 video_nr=5 card_label="DV Webcam"
```

To check that list of created devices use

```bash
v4l2-ctl --list-devices
```

3. Run the sample

```bash
mkdir build && cd build
cmake ..
make
./webcam_stream
```

4. To remove the virtual device run

```bash
sudo modprobe --remove v4l2loopback
```