#include <dv-processing/io/camera_capture.hpp>
#include <dv-processing/visualization/event_visualizer.hpp>
#include <opencv2/highgui.hpp>
#include <sys/ioctl.h>
#include <linux/videodev2.h>

#include <iostream>

#define VIDEO_OUT "/dev/video5"

int main(){
    dv::io::CameraCapture capture;
    const cv::Size resolution = capture.getEventResolution().value();

    dv::visualization::EventVisualizer visualizer(resolution);

    int outputFile = open(VIDEO_OUT, O_RDWR);
    if(outputFile < 0){
        std::cerr<<"ERROR: Failed to open output file\n";
        return EXIT_FAILURE;
    }

    struct v4l2_format vid_format;
    memset(&vid_format, 0, sizeof(vid_format));
    vid_format.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;

    if(ioctl(outputFile, VIDIOC_G_FMT, &vid_format) < 0){
        std::cerr<<"ERROR: unable to get video format\n";
        return EXIT_FAILURE;
    }

    const size_t frameSize = resolution.width*resolution.height*3;
    vid_format.fmt.pix.width = resolution.width;
    vid_format.fmt.pix.height = resolution.height;
    vid_format.fmt.pix.pixelformat = V4L2_PIX_FMT_RGB24;
    vid_format.fmt.pix.sizeimage = frameSize;
    vid_format.fmt.pix.field = V4L2_FIELD_NONE;

    if(ioctl(outputFile, VIDIOC_S_FMT, &vid_format) < 0){
        std::cerr<<"ERROR: unable to set video format\n";
        return EXIT_FAILURE;
    }

    bool keepRunning = true;
    while(capture.isRunning() && keepRunning){
        const auto eventBatch = capture.getNextEventBatch();
        if(!eventBatch.has_value()){
            continue;
        }
        cv::Mat image = visualizer.generateImage(*eventBatch);
        cv::cvtColor(image, image, cv::COLOR_BGR2RGB);

        const auto numWrittenBytes = write(outputFile, image.data, frameSize);
        if(numWrittenBytes < 0){
            std::cerr<<"ERROR: Failed to write to output device\n";
            close(outputFile);
            keepRunning = false;
        }

        cv::imshow("Check", visualizer.generateImage(*eventBatch));
        if(cv::waitKey(1) == 27){
            keepRunning = false;
        }
    }
    return EXIT_SUCCESS;
}